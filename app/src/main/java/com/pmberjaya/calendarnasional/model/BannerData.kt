package com.pmberjaya.calendarnasional.model

/**
 * Created by Exel staderlin on 07-Apr-17.
 */

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BannerData {

    @SerializedName("id")
    @Expose
    val id: String? = null
    @SerializedName("title")
    @Expose
    val title: String? = null
    @SerializedName("banner")
    @Expose
    val banner: String? = null
    @SerializedName("url")
    @Expose
    val url: String? = null
}