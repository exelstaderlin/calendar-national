package com.pmberjaya.calendarnasional.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class HariLiburData(
    @SerializedName("tanggal")
    @Expose
    val tanggal: String? = null,
    @SerializedName("title")
    @Expose
    val title: String? = null,
    @SerializedName("state")
    @Expose
    val state: String? = null
)

