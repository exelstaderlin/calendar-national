package com.pmberjaya.calendarnasional.utilities

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.pmberjaya.calendarnasional.model.HariLiburData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken






/**
 * Created by Exel staderlin on 03/08/2016.
 */

const val SESSION = "session"
const val BASE_URL = "base_url"
const val VERSION = "version"
const val NATION = "nation"
const val CALENDAR_INDONESIA = "calendar_indonesia"
const val CALENDAR_MALAYSIA = "calendar_malaysia"
const val CALENDAR_SINGAPORE = "calendar_singapore"
const val CALENDAR_THAILAND = "calendar_thailand"
const val CALENDAR_MYANMAR = "calendar_myanmar"
const val CALENDAR_VIETNAM = "calendar_vietnam"
const val CALENDAR_CAMBODIA = "calendar_cambodia"

class SessionManager(context: Context) {

    private val mSharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    private val mEditor: SharedPreferences.Editor

    init {
        mEditor = mSharedPreferences.edit()
        mEditor.apply()
    }

    var nation: String?
        get() = mSharedPreferences.getString(NATION, "Indonesia")
        set(value) {
            mEditor.putString(NATION, value)
            mEditor.apply()
        }


    var indonesia: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_INDONESIA, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_INDONESIA, json)
            mEditor.apply()
        }

    var malaysia: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_MALAYSIA, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_MALAYSIA, json)
            mEditor.apply()
        }

    var singapore: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_SINGAPORE, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_SINGAPORE, json)
            mEditor.apply()
        }

    var thailand: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_THAILAND, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_THAILAND, json)
            mEditor.apply()
        }

    var vietnam: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_VIETNAM, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_VIETNAM, json)
            mEditor.apply()
        }

    var myanmar: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_MYANMAR, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_MYANMAR, json)
            mEditor.apply()
        }

    var cambodia: List<HariLiburData>
        get() {
            val gson = Gson()
            val hariLibur: List<HariLiburData>
            val type = object : TypeToken<List<HariLiburData>>() {}.type
            hariLibur = gson.fromJson(mSharedPreferences.getString(CALENDAR_CAMBODIA, ""), type)
            return hariLibur
        }
        set(value) {
            val gson = Gson()
            val json = gson.toJson(value)
            mEditor.putString(CALENDAR_CAMBODIA, json)
            mEditor.apply()
        }

    var hasSession: Boolean
        get() = mSharedPreferences.getBoolean(SESSION, false)
        set(value) {
            mEditor.putBoolean(SESSION, value)
            mEditor.apply()
        }

    var version: String
        get() = mSharedPreferences.getString(VERSION, "0") ?: "0"
        set(value) {
            mEditor.putString(VERSION, value)
            mEditor.apply()
        }



    fun clearNation() {
        mEditor.remove(NATION)
        mEditor.apply()
    }

    fun clearSession() {
        mEditor.clear()
        mEditor.apply()
    }

    private enum class SWITCH {
        SLEEP_TRACKING,
        EQUALIZER,
        SHAKE_AND_GESTURE
    }

}