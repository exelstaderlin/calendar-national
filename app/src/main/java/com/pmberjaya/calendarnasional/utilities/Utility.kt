package com.pmberjaya.calendarnasional.utilities


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.Uri
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import org.json.JSONArray
import java.io.IOException
import kotlin.collections.ArrayList
import android.telephony.TelephonyManager
import java.util.*


/**
 * Created by Exel staderlin on 23/07/2016.
 */
object Utility {

    fun randomInt(range : Int) : Int {
        val rnd = Random()
        return rnd.nextInt(range)
    }

    fun getUserCountry(context: Context): String? {
        try {
            val tm = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
            val simCountry = tm.simCountryIso // Lokasi SIM
            val networkCountry = tm.networkCountryIso   // Lokasi Network
            return if (simCountry != null && simCountry.length == 2) { // SIM country code is available
                simCountry.toLowerCase(Locale.getDefault())
            } else {
                networkCountry.toUpperCase(Locale.getDefault())
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    private fun readFileJson(context: Context, fileName : String): JSONArray? {
        var jsonObject: JSONArray? = null
        try {
            val `is` = context.assets.open("calendar_indonesia_2019.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            val json = String(buffer)
            jsonObject = JSONArray(json)

        } catch (ex: IOException) {
            ex.printStackTrace()
        }

        return jsonObject
    }

    fun checkInternetOn(context: Context): Boolean {
        val localNetwork =
            (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).activeNetworkInfo
        return localNetwork != null && localNetwork.isAvailable
    }

    fun grantAllUriPermissions(context: Context, intent: Intent, uri: Uri?) {
        val resInfoList = context.packageManager.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY)
        for (resolveInfo in resInfoList) {
            val packageName =
                resolveInfo.activityInfo.packageName  //Comment: Give permission utk membuka file pdf di nougat API 24 versi android 7.0
            context.grantUriPermission(
                packageName,
                uri,
                Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
        }
    }

    fun checkThePermission(context: Context): Boolean {
        val permission = ArrayList<String>()
        val result = ContextCompat.checkSelfPermission(context, android.Manifest.permission.READ_EXTERNAL_STORAGE)
        val result2 = ContextCompat.checkSelfPermission(context, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)

        when (result) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.READ_EXTERNAL_STORAGE)
        }
        when (result2) {
            PackageManager.PERMISSION_DENIED -> permission.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
        }

        return if (permission.size != 0) {
            requestPermission(context, permission)
            false
        } else true
    }

    private fun requestPermission(context: Context, permission: ArrayList<String>) {
        val arrayPermit = arrayOfNulls<String>(permission.size)
        permission.toArray(arrayPermit)
        ActivityCompat.requestPermissions(context as Activity, permission.toTypedArray(), 1)
    }

}