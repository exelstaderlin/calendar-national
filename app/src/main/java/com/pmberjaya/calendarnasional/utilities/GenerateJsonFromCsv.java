package com.pmberjaya.calendarnasional.utilities;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.univocity.parsers.common.processor.RowListProcessor;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import com.univocity.parsers.tsv.TsvParser;
import com.univocity.parsers.tsv.TsvParserSettings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class GenerateJsonFromCsv {


    public static void main(String[] args) {
        Timer timer = runTimer();
        ArrayList<List<String>> datasetV1 = readCsvToList("D:/calendar_cambodia.tsv");
        writeText(datasetV1);
        timer.cancel();
    }


    private static void writeText(ArrayList<List<String>> datasetV1) {
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter("D://calendar_cambodia.json");
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter printWriter = new PrintWriter(fileWriter);
        JsonArray arrayCalendar = new JsonArray();

        for (List<String> data : datasetV1) {
            JsonObject dataCalendar = new JsonObject();
            dataCalendar.addProperty("tanggal", convertDateString(data.get(1)));
            dataCalendar.addProperty("title", data.get(0));
            dataCalendar.addProperty("state", "National");
            arrayCalendar.add(dataCalendar);
        }
        printWriter.println(arrayCalendar.toString());
        printWriter.close();
    }

    private static String convertDateString(String strDate) {
        String tanggal = strDate;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy MMM dd", Locale.getDefault());
        SimpleDateFormat fmtOut = null;
        Date date = null;
        try {
            date = fmt.parse(tanggal);
            fmtOut = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            System.out.println(fmtOut.format(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert fmtOut != null;
        return fmtOut.format(date);
    }


    private static ArrayList<List<String>> readCsvToList(String csvFileLocation) {
        ArrayList<List<String>> records = new ArrayList<>();
        TsvParser parser = new TsvParser(settingsCsvParser());
        parser.beginParsing(new File(csvFileLocation));
        try {
            String[] row;
            int loop = 1;
            int threshold = 5000000;
            while ((row = parser.parseNext()) != null) {
                records.add(Arrays.asList(row));
                if (loop >= threshold) {
                    System.out.println("load record :" + loop);
                }
                loop++;
            }
            System.out.println("record size :" + records.size());
            return records;
        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private static TsvParserSettings  settingsCsvParser() {
        TsvParserSettings  settings = new TsvParserSettings(); // you'll find many options here, check the tutorial.
        RowListProcessor rowProcessor = new RowListProcessor();
        settings.setNullValue("<NULL>");
//        settings.setEmptyValue("<EMPTY>");
        settings.setIgnoreLeadingWhitespaces(false);
        settings.getFormat().setLineSeparator("\n");
        settings.setIgnoreTrailingWhitespaces(false);
        settings.setNumberOfRecordsToRead(10000000);
        settings.setSkipEmptyLines(false);
        settings.setMaxCharsPerColumn(10000000);
        settings.setInputBufferSize(1000);
        settings.setReadInputOnSeparateThread(false);
        settings.setLineSeparatorDetectionEnabled(true);
        settings.setRowProcessor(rowProcessor);
        settings.setHeaderExtractionEnabled(true);
        return settings;
    }

    private static Timer runTimer() {
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            private int i = 0;

            public void run() {
                System.out.println("time running :" + i++); /*difference time*/
            }
        };
        timer.scheduleAtFixedRate(task, 0, 1000); //1000ms = 1sec
        return timer;
    }
}
