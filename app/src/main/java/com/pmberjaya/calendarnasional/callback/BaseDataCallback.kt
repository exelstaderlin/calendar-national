package com.pmberjaya.calendarnasional.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class  BaseDataCallback<T>{

    @SerializedName("status")
    @Expose
    var status: Boolean = true

    @SerializedName("data")
    @Expose
    var data: T? = null

    @SerializedName("messages")
    @Expose
    var message: List<String> = emptyList()

    @Deprecated("tidak perlu pagination")
    @SerializedName("total")
    @Expose
    var total: String? = null

}