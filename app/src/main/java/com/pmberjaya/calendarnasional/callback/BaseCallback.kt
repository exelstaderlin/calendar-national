package com.pmberjaya.calendarnasional.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

open class BaseCallback {

    @SerializedName("status")
    @Expose
    var status: Boolean? = null

    @SerializedName("error")
    @Expose
    var error: List<String>? = ArrayList()

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("access_token")
    @Expose
    var access_token: String? = null

    @SerializedName("title")
    @Expose
    private var title: String? = null

    @SerializedName("action")
    @Expose
    var action: String ?= null

}