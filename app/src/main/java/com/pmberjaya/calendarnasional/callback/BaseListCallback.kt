package com.pmberjaya.calendarnasional.callback

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*


class BaseListCallback<T> {
    @SerializedName("status")
    @Expose
    var status: Boolean = false

    @SerializedName("data")
    @Expose
    var data: ArrayList<T> = ArrayList()

    @SerializedName("total")
    @Expose
    var total: Int? = null

    @SerializedName("messages")
    @Expose
    var messages: List<String>? = ArrayList()

    @SerializedName("url_share")
    @Expose
    var urlShare: String? = null

}