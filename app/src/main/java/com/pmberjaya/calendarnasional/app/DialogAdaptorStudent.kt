package com.pmberjaya.calendarnasional.app

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import com.pmberjaya.calendarnasional.R

import java.util.ArrayList

internal class DialogAdaptorStudent(
    private val context: Activity,
    private val alCustom: ArrayList<Dialogpojo>
) : BaseAdapter() {

    override fun getCount(): Int {
        return alCustom.size
    }

    override fun getItem(i: Int): Any {
        return alCustom[i]
    }

    override fun getItemId(i: Int): Long {
        return i.toLong()
    }

    @SuppressLint("SetTextI18n", "ViewHolder")
    @TargetApi(Build.VERSION_CODES.O)
    override fun getView(position: Int, convertView: View, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val listViewItem = inflater.inflate(R.layout.row_addapt, null, true)

        val tvTitle = listViewItem.findViewById<View>(R.id.tv_name) as TextView
        val tvSubject = listViewItem.findViewById<View>(R.id.tv_type) as TextView
        val tvDescription = listViewItem.findViewById<View>(R.id.tv_desc) as TextView


        tvTitle.text = "Judul : " + alCustom[position].titles!!
        tvSubject.text = "States : " + alCustom[position].subjects!!
        tvDescription.text = "Deksripsi : " + alCustom[position].descripts!!

        return listViewItem
    }

}

