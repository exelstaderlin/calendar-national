package com.pmberjaya.calendarnasional.app

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pmberjaya.calendarnasional.R

/**
 * Created by Exel staderlin on 10/30/2019.
 */
class HolidayAdapter(var data : List<String>) : RecyclerView.Adapter<HolidayAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.calendar_holiday_layout, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.hariLibur.text = data[position]
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var hariLibur: TextView = itemView.findViewById<View>(R.id.hari_libur_tv) as TextView
    }
}