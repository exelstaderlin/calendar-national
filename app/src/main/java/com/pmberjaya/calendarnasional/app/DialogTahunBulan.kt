package com.pmberjaya.calendarnasional.app

import android.annotation.SuppressLint
import android.app.Activity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.pmberjaya.calendarnasional.R

/**
 * Created by Exel staderlin on 10/30/2019.
 */
class DialogTahunBulan(var context: Activity, var yearString : String,var monthString : String) {

    private val listYear = arrayListOf("2016", "2017", "2018","2019", "2020", "2021", "2022")
    private val listMonth = arrayListOf(
        "January",
        "Febuary",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December"
    )

    private var finalYear = ""
    private var finalMonth = 0

    private val yearAdapter = ArrayAdapter(
        context, R.layout.spinner_item, listYear
    )
    private val monthAdapter = ArrayAdapter(
        context, R.layout.spinner_item, listMonth
    )

    @SuppressLint("InflateParams")
    fun showFilterTicketDialog(onValid: (year: Int, month: Int) -> Unit) {
        val dialog: AlertDialog
        val builder = AlertDialog.Builder(context)
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_tahun_bulan, null)

        val spinnerYear = dialogView.findViewById<Spinner>(R.id.spinner_year)
        val spinnerMonth = dialogView.findViewById<Spinner>(R.id.spinner_month)
        val ok = dialogView.findViewById<TextView>(R.id.positive_button)
        val cancel = dialogView.findViewById<TextView>(R.id.negative_button)

        builder.setView(dialogView)
        setSpinnerAdapter(spinnerYear, spinnerMonth)
        setSpinText(spinnerYear, yearString)
        setSpinText(spinnerMonth, monthString)

        dialog = builder.create()
        dialog.show()
        dialog.setCancelable(false)
        ok.setOnClickListener {
            dialog.dismiss()
            onValid(Integer.valueOf(finalYear), finalMonth)
        }
        cancel.setOnClickListener {
            dialog.dismiss()
        }
    }

    private fun setSpinText(spin: Spinner, text: String) {
        for (i in 0 until spin.adapter.count) {
            if (spin.adapter.getItem(i).toString().contains(text)) {
                spin.setSelection(i)
            }
        }
    }

    private fun setSpinnerAdapter(spinnerYear: Spinner, spinnerMonth: Spinner) {

        spinnerYear.adapter = yearAdapter
        spinnerMonth.adapter = monthAdapter

        spinnerYear.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                finalYear =  adapterView.selectedItem.toString()
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }
        spinnerMonth.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(adapterView: AdapterView<*>, view: View, i: Int, l: Long) {
                finalMonth = i
            }

            override fun onNothingSelected(adapterView: AdapterView<*>) {}
        }
    }
}