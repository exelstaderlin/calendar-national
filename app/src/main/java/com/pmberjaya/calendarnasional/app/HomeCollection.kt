package com.pmberjaya.calendarnasional.app

data class HomeCollection(var tanggal: String, var title: String, var state: String, var description: String) {

    companion object {
        var date_collection_arr: ArrayList<HomeCollection> = ArrayList()
    }
}
