package com.pmberjaya.calendarnasional.app

import android.annotation.SuppressLint
import android.app.Activity
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.pmberjaya.calendarnasional.R


/**
 * Created by Exel staderlin on 10/30/2019.
 */

@SuppressLint("SetTextI18n")
class DialogInformationHoliday(
    var context: Activity,
    homeCollection: ArrayList<HomeCollection>,
    date: String
) : AlertDialog.Builder(context) {

    init {
        val dialog: AlertDialog = this.create()
        val inflater = context.layoutInflater
        val dialogView = inflater.inflate(R.layout.dialog_inform, null)
        val title = dialogView.findViewById<TextView>(R.id.tv_name)
        val state = dialogView.findViewById<TextView>(R.id.tv_type)
        val tvDate = dialogView.findViewById<TextView>(R.id.tv_date)
        this.setView(dialogView)

        for (j in 0 until homeCollection.size) {
            if (homeCollection[j].tanggal == date) {
                title.text = "Title : " + homeCollection[j].title
                state.text = "State : " + homeCollection[j].state
                tvDate.text = "Date : $date"
                break
            }
        }

        dialog.setCancelable(false)
    }
}