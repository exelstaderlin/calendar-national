package com.pmberjaya.calendarnasional.app

import androidx.lifecycle.ViewModel
import com.pmberjaya.calendarnasional.callback.BaseListCallback
import com.pmberjaya.calendarnasional.io.BaseDataRx
import com.pmberjaya.calendarnasional.io.BaseRx
import com.pmberjaya.calendarnasional.io.MutableLiveDataInitialization
import com.pmberjaya.calendarnasional.io.RestClient
import com.pmberjaya.calendarnasional.model.BannerData
import com.pmberjaya.calendarnasional.model.HariLiburData
import io.reactivex.observers.DisposableObserver

/**
 * Created by Exel staderlin on 7/25/2019.
 */
class MainViewModel : ViewModel() {

    var checkVersion by MutableLiveDataInitialization<String>()
    var indonesiaResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var singaporeResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var malaysiaResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var thailandResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var myanmarResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var vietnamResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var cambodiaResponse by MutableLiveDataInitialization<List<HariLiburData>>()
    var errorResponse by MutableLiveDataInitialization<Throwable>()
    var bannerResponse by MutableLiveDataInitialization<BaseListCallback<BannerData>>()

    fun checkVersionDate() {
        val observable = RestClient.getMainInterface("").checkVersion()
        BaseRx<String>().requestNoList(
            observable,
            object : DisposableObserver<String>() {
                override fun onComplete() {
                }

                override fun onNext(t: String) {
                    checkVersion.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getIndonesiaJson(token: String) {
        val observable = RestClient.getMainInterface(token).getIndonesiaJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    indonesiaResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getSingaporeJson(token: String) {
        val observable = RestClient.getMainInterface(token).getSingaporeJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    singaporeResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getMalaysiaJson(token: String) {
        val observable = RestClient.getMainInterface(token).getMalaysiaJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    malaysiaResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getThailandJson(token: String) {
        val observable = RestClient.getMainInterface(token).getThailandJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    thailandResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }


    fun getMyanmarJson(token: String) {
        val observable = RestClient.getMainInterface(token).getMyanmarJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    myanmarResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getVietnamJson(token: String) {
        val observable = RestClient.getMainInterface(token).getVietnamJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    vietnamResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

    fun getCambodiaJson(token: String) {
        val observable = RestClient.getMainInterface(token).getCambodiaJson()
        BaseRx<List<HariLiburData>>().requestNoList(
            observable,
            object : DisposableObserver<List<HariLiburData>>() {
                override fun onComplete() {
                }

                override fun onNext(t: List<HariLiburData>) {
                    cambodiaResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }


    fun getBanner() {
        val apiInterface = RestClient.getApiSariputta().getBanner()
        BaseDataRx<BannerData>().requestList(
            apiInterface,
            object : DisposableObserver<BaseListCallback<BannerData>>() {
                override fun onComplete() {}
                override fun onNext(t: BaseListCallback<BannerData>) {
                    bannerResponse.value = t
                }

                override fun onError(e: Throwable) {
                    errorResponse.value = e
                }
            })
    }

}