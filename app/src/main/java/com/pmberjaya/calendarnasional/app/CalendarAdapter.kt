package com.pmberjaya.calendarnasional.app

import android.app.Activity
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pmberjaya.calendarnasional.R
import org.json.JSONArray
import org.json.JSONException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Exel staderlin on 10/23/2019.
 */

class CalendarAdapter(
    private var context: Activity,
    private var month: GregorianCalendar
) :
    RecyclerView.Adapter<CalendarAdapter.ViewHolder>() {

    private val selectedDate = month.clone() as GregorianCalendar
    var firstDay: Int = 0
    var df: DateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    var mListTanggalString: ArrayList<String> = ArrayList()
    var mListHariString: ArrayList<String> = ArrayList()
    private var gridvalue: String = ""
    private var homeCollection: ArrayList<HomeCollection> = HomeCollection.date_collection_arr

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        initObject()
        val v = LayoutInflater.from(context).inflate(R.layout.cal_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val separatedTime = mListTanggalString[position].split("-".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

        gridvalue = separatedTime[2].replaceFirst("^0*".toRegex(), "")
        if (Integer.parseInt(gridvalue) > 1 && position < firstDay) {
            holder.dayTextView.setTextColor(Color.parseColor("#A9A9A9"))
            holder.dayTextView.isClickable = false
            holder.dayTextView.isFocusable = false
        } else if (Integer.parseInt(gridvalue) < 7 && position > 28) {
            holder.dayTextView.setTextColor(Color.parseColor("#A9A9A9"))
            holder.dayTextView.isClickable = false
            holder.dayTextView.isFocusable = false
        } else {
            // setting curent month's days in blue color.
            holder.dayTextView.setTextColor(Color.parseColor("#696969"))
        }

        val currentDateString = (df as SimpleDateFormat).format(selectedDate.time)
        if (mListTanggalString[position] == currentDateString) {
            holder.view.setBackgroundColor(context.resources.getColor(R.color.green))
        } else {
            holder.view.setBackgroundColor(context.resources.getColor(R.color.white))
        }
        if (mListHariString[position] == "Sunday") {
            holder.view.setBackgroundColor(context.resources.getColor(R.color.red))
            holder.dayTextView.setTextColor(context.resources.getColor(R.color.white))
        }
        holder.dayTextView.text = gridvalue
        setEventView(holder.view, position, holder.dayTextView)
    }

    override fun getItemCount(): Int {
        return mListTanggalString.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayTextView: TextView = itemView.findViewById<View>(R.id.date) as TextView

        var view = itemView
        init {
            itemView.setOnClickListener {
                val day = mListTanggalString[adapterPosition]
                DialogInformationHoliday(context, homeCollection, day).show()
            }
        }
    }

    private fun initObject() {
        Locale.setDefault(Locale.US)
        month.set(GregorianCalendar.DAY_OF_MONTH, 1)
        refreshDays()
    }

    fun refreshDays() {
        // clear items
        mListTanggalString.clear()
        mListHariString.clear()
        Locale.setDefault(Locale.getDefault())
        // month start day. ie; sun, mon, etc
        firstDay = month.get(GregorianCalendar.DAY_OF_WEEK)
        // finding number of weeks in current month.
        val maxWeekNumber = month.getActualMaximum(GregorianCalendar.WEEK_OF_MONTH)
        // allocating maximum row number for the gridview.
        val monthLength = maxWeekNumber * 7
        val calMaxP = getMaxMonth() - (firstDay - 1)// calendar offday starting 24,25 ...
        val monthMaxSet = month.clone() as GregorianCalendar
        monthMaxSet.set(GregorianCalendar.DAY_OF_MONTH, calMaxP + 1) // tambah 1 agar tgl 31 tetap muncul jika ada
        monthMaxSet.set(GregorianCalendar.MONTH, month.get(GregorianCalendar.MONTH) - 1) // agar bulan dimulai dari 1 bkn 0
        for (n in 0 until monthLength) {
            val tanggalValue = df.format(monthMaxSet.time)
            val dayValue = monthMaxSet.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
            monthMaxSet.add(GregorianCalendar.DATE, 1)
            mListTanggalString.add(tanggalValue)
            mListHariString.add(dayValue)
        }
    }

    private fun getMaxMonth(): Int {
        val pMonth = month.clone() as GregorianCalendar
        if (month.get(GregorianCalendar.MONTH) == month
                .getActualMinimum(GregorianCalendar.MONTH)
        ) {
            pMonth.set(
                month.get(GregorianCalendar.YEAR) - 1,
                month.getActualMaximum(GregorianCalendar.MONTH), 1
            )
        } else {
            pMonth.set(
                GregorianCalendar.MONTH,
                month.get(GregorianCalendar.MONTH) - 1
            )
        }
        val maxMonth = pMonth.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
        return maxMonth
    }

    private fun setEventView(v: View, pos: Int, txt: TextView) {
        for (i in 0 until homeCollection.size) {
            val homeCollection = homeCollection[i]
            val date = homeCollection.tanggal
            val dayStringSize = mListTanggalString.size
            if (dayStringSize > pos) {
                if (mListTanggalString[pos] == date) {
                    if (Integer.parseInt(gridvalue) > 1 && pos < firstDay) {
                        /* Do Nothing */
                    } else if (Integer.parseInt(gridvalue) < 7 && pos > 28) {
                        /* Do Nothing */
                    } else {
                        v.setBackgroundColor(context.resources.getColor(R.color.red))
                        txt.setTextColor(context.resources.getColor(R.color.white))
                    }

                }
            }
        }
    }

    private fun getMatchList(detail: String): ArrayList<Dialogpojo> {
        val jsonArray = JSONArray(detail)
        val alCustom = ArrayList<Dialogpojo>()
        try {
            for (i in 0 until jsonArray.length()) {

                val jsonObject = jsonArray.optJSONObject(i)

                val pojo = Dialogpojo()

                pojo.titles = jsonObject.optString("hnames")
                pojo.subjects = jsonObject.optString("hsubject")
                pojo.descripts = jsonObject.optString("descript")

                alCustom.add(pojo)

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return alCustom
    }
}