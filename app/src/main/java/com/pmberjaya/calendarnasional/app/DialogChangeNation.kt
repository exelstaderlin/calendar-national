package com.pmberjaya.calendarnasional.app

import android.R
import android.content.Context
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.pmberjaya.calendarnasional.utilities.SessionManager

/**
 * Created by Exel staderlin on 10/31/2019.
 */
class DialogChangeNation(context: Context, onSelected: () -> Unit) : AlertDialog.Builder(context) {

    private val arrData = arrayListOf("Indonesia", "Singapore", "Malaysia", "Thailand", "Vietnam", "Myanmar", "Cambodia")
    private val sessionManager = SessionManager(context)
    private val checkedItem = when (sessionManager.nation) {
        "Indonesia" -> 0
        "Singapore" -> 1
        "Malaysia" -> 2
        "Thailand" -> 3
        "Vietnam" -> 4
        "Myanmar" -> 5
        "Cambodia" -> 6
        else -> 0
    }

    private val adapter = ArrayAdapter(
        context, R.layout.select_dialog_singlechoice, arrData
    )

    init {
        this.setSingleChoiceItems(adapter, checkedItem) { dialog, item ->
            val nation = arrData[item]
            sessionManager.nation = nation
            onSelected()
            dialog.dismiss()
        }
        this.create()
    }

}