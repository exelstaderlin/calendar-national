package com.pmberjaya.calendarnasional.app

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.text.format.DateFormat.format
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.pmberjaya.calendarnasional.R
import com.pmberjaya.calendarnasional.model.HariLiburData
import com.pmberjaya.calendarnasional.utilities.SessionManager
import com.pmberjaya.calendarnasional.utilities.Utility
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.calendar_layout.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class MainActivity : AppCompatActivity() {
    private lateinit var mCalMonth: GregorianCalendar
    private lateinit var mCalendarAdapter: CalendarAdapter
    private lateinit var mHolidayAdapter: HolidayAdapter
    private lateinit var mDateFormat: DateFormat
    private var url: String? = null


    //    private var mMenu: Menu? = null
    private var mSessionManager: SessionManager? = null
    private var mViewModel: MainViewModel? = null

//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        this.mMenu = menu
//        menuInflater.inflate(R.menu.main_menu, menu)
//        firstInitFlag()
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem): Boolean {
//        return when (item.itemId) {
//            R.id.setting -> {
//                DialogChangeNation(this) {
//                    val flagNation = changeNationFlag()
//                    mMenu?.findItem(item.itemId)?.icon = flagNation
//                    getNationHariLibur()
//                }.show()
//                true
//            }
//            else -> super.onOptionsItemSelected(item)
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initObject()
        initListener()
        initObserver()
        getBannerIklan()
        setAdapter()
        firstInitNationality()
        if(mSessionManager?.version != "0")
            setAllCalendar()
        checkUpdateDate()
    }

    private fun initObject() {
        mCalMonth = GregorianCalendar.getInstance() as GregorianCalendar
        mSessionManager = SessionManager(this)
        mDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        mViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
        tv_month.text = format("MMMM yyyy", mCalMonth)
        tv_today.text = format("EEEE, MMMM dd, yyyy", mCalMonth)
    }

    private fun initListener() {
        ib_prev.setOnClickListener {
            if (mCalMonth.get(GregorianCalendar.MONTH) == Calendar.MAY && mCalMonth.get(
                    GregorianCalendar.YEAR
                ) == 2017
            ) {
                //cal_month.set((cal_month.get(GregorianCalendar.YEAR) - 1), cal_month.getActualMaximum(GregorianCalendar.MONTH), 1);
                Toast.makeText(
                    this@MainActivity,
                    "Event Detail is available for current session only.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                setPreviousMonth()
                refreshCalendar()
                refreshHoliday()
            }
        }

        Ib_next.setOnClickListener {
            if (mCalMonth.get(GregorianCalendar.MONTH) == Calendar.JUNE && mCalMonth.get(
                    GregorianCalendar.YEAR
                ) == 2018
            ) {
                //cal_month.set((cal_month.get(GregorianCalendar.YEAR) + 1), cal_month.getActualMinimum(GregorianCalendar.MONTH), 1);
                Toast.makeText(
                    this@MainActivity,
                    "Event Detail is available for current session only.",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                setNextMonth()
                refreshCalendar()
                refreshHoliday()
            }
        }

        nation.setOnClickListener {
            DialogChangeNation(this) {
                val flagNation = changeNationFlag()
                nation.background = flagNation
                setAllCalendar()
            }.show()
        }

        close!!.setOnClickListener { iklan!!.visibility = View.GONE }

        imageBanner!!.setOnClickListener {
            try {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
            } catch (anfe: ActivityNotFoundException) {
                Toast.makeText(this, anfe.toString(), Toast.LENGTH_SHORT).show()
            }
        }

        tv_month.setOnClickListener {
            val yearString = format("yyyy", mCalMonth)
            val monthString = format("MMMM", mCalMonth)
            DialogTahunBulan(
                this,
                yearString as String,
                monthString as String
            ).showFilterTicketDialog { year, month ->
                mCalMonth.set(year, month, 1)
                refreshCalendar()
                refreshHoliday()
            }
        }
    }

    private fun initObserver() {
        var allLoaded = 0
        mViewModel?.indonesiaResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.indonesia = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.singaporeResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.singapore = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.malaysiaResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.malaysia = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.thailandResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.thailand = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.myanmarResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.myanmar = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.vietnamResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.vietnam = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.cambodiaResponse?.observe(this, Observer { data ->
            progress_bar.visibility = View.GONE
            mSessionManager?.cambodia = data
            allLoaded++
            if (allLoaded == 7) setAllCalendar()
        })

        mViewModel?.checkVersion?.observe(this, Observer { version ->
            val currentVersion = mSessionManager?.version!!
            val boolean = currentVersion < version
            if (boolean && mSessionManager?.version != "0")
                setAllCalendar()
            else
                getNationHariLibur()

            mSessionManager?.version = version
        })

        mViewModel?.bannerResponse?.observe(this, Observer {
            if (it?.status!!) {
                if (it.data.size != 0) {
                    val randomInt = Utility.randomInt(it.data.size) //Random number
                    url = it.data[randomInt].url
                    setBannerImage(it.data[randomInt].banner)
                    iklan!!.visibility = View.VISIBLE
                }
            }
        })

        mViewModel?.errorResponse?.observe(this, Observer {
            progress_bar.visibility = View.GONE
            Toast.makeText(this, it.toString(), Toast.LENGTH_LONG).show()
            Log.d("Error API : ", it.toString())
        })
    }

    private fun initAds() {
        MobileAds.initialize(this) {}
        val adRequestBuilder = AdRequest.Builder()
        adRequestBuilder.addTestDevice("EAF38382A6F90587D4F22CFE334736FA")
        val adRequest = adRequestBuilder.build()
        adView.loadAd(adRequest)
        adView.visibility = View.VISIBLE
    }

    private fun checkUpdateDate() {
        if (Utility.checkInternetOn(this))
            mViewModel?.checkVersionDate()

    }

    private fun setAllCalendar() {
        HomeCollection.date_collection_arr.clear() // di clear biar gk double tanggal liburnya
        when (mSessionManager?.nation) {
            "Indonesia" -> mSessionManager?.indonesia?.let { initHomeCollection(it) }
            "Singapore" -> mSessionManager?.singapore?.let { initHomeCollection(it) }
            "Malaysia" -> mSessionManager?.malaysia?.let { initHomeCollection(it) }
            "Thailand" -> mSessionManager?.thailand?.let { initHomeCollection(it) }
            "Vietnam" -> mSessionManager?.vietnam?.let { initHomeCollection(it) }
            "Myanmar" -> mSessionManager?.myanmar?.let { initHomeCollection(it) }
            "Cambodia" -> mSessionManager?.cambodia?.let { initHomeCollection(it) }
        }
        refreshCalendar()
        refreshHoliday()
    }

    private fun getNationHariLibur() {
        if (Utility.checkInternetOn(this)) {
            progress_bar.visibility = View.VISIBLE
            mViewModel?.getIndonesiaJson("")
            mViewModel?.getSingaporeJson("")
            mViewModel?.getMalaysiaJson("")
            mViewModel?.getThailandJson("")
            mViewModel?.getMyanmarJson("")
            mViewModel?.getVietnamJson("")
            mViewModel?.getCambodiaJson("")
        } else {
            progress_bar.visibility = View.GONE
            Toast.makeText(this, "No internet connection, please try again", Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun getBannerIklan() {
        when (Utility.randomInt(2)) {
            0 -> if (Utility.checkInternetOn(this)) {
                mViewModel?.getBanner()
            }
            1 -> initAds()
        }
    }

    private fun setBannerImage(banner: String?) {
        val options = RequestOptions()
            .centerCrop()
            .error(R.drawable.img_noimage)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)

        Glide.with(this)
            .load(banner)
            .apply(options)
            .into(imageBanner)
    }

    private fun setAdapter() {
        mCalendarAdapter = CalendarAdapter(this, mCalMonth)
        recycle_calendar.layoutManager = GridLayoutManager(this, 7)
        recycle_calendar.addItemDecoration(
            DividerItemDecoration(
                recycle_calendar.context,
                DividerItemDecoration.VERTICAL
            )
        ) // Untuk Membentuk Garis Pemisah
        recycle_calendar.addItemDecoration(
            DividerItemDecoration(
                recycle_calendar.context,
                DividerItemDecoration.HORIZONTAL
            )
        ) // Untuk Membentuk Garis Pemisah
        recycle_calendar.adapter = mCalendarAdapter

        val currentDate = mDateFormat.format(mCalMonth.time)
        val listHoliday = getListHoliday(currentDate)
        mHolidayAdapter = HolidayAdapter(listHoliday)
        recycle_holiday.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycle_holiday.adapter = mHolidayAdapter
    }

    private fun firstInitNationality() {
        val country = Utility.getUserCountry(this)
        when (country) {
            "ID" -> mSessionManager?.nation = "Indonesia"
            "MY" -> mSessionManager?.nation = "Malaysia"
            "SG" -> mSessionManager?.nation = "Singapore"
            "TH" -> mSessionManager?.nation = "Thailand"
            "MM" -> mSessionManager?.nation = "Myanmar"
            "VN" -> mSessionManager?.nation = "Vietnam"
            "KH" -> mSessionManager?.nation = "Cambodia"
            else -> mSessionManager?.nation = "Indonesia"
        }
        val flagNation = changeNationFlag()
        nation.background = flagNation
    }

    private fun initHomeCollection(listHariLiburData: List<HariLiburData>) {
        println("length json = " + listHariLiburData.size)
        for (i in listHariLiburData.indices) {
            val tanggal = listHariLiburData[i].tanggal ?: ""
            val title = listHariLiburData[i].title ?: ""
            val state = listHariLiburData[i].state ?: ""
            val deskripsi = "Selamat Hari Libur Nasional"
            val homeCollection = HomeCollection(
                tanggal,
                title,
                state,
                deskripsi
            )
            HomeCollection.date_collection_arr.add(homeCollection)
        }
    }

    private fun getListHoliday(currentDate: String): List<String> {
        val listHoliday = ArrayList<String>()
        val holidayCollection =
            HomeCollection.date_collection_arr
        for (i in 0 until holidayCollection.size) {
            val tanggal = holidayCollection[i].tanggal
            val title = holidayCollection[i].title
            if (tanggal.substring(0, 7) == currentDate.substring(
                    0,
                    7
                )
            ) { // tahun-bulan == tahun-bulan
                listHoliday.add("$tanggal : $title")
            }
        }
        return listHoliday
    }

    private fun setNextMonth() {
        if (mCalMonth.get(GregorianCalendar.MONTH) == mCalMonth.getActualMaximum(GregorianCalendar.MONTH)) {
            mCalMonth.set(
                mCalMonth.get(GregorianCalendar.YEAR) + 1,
                mCalMonth.getActualMinimum(GregorianCalendar.MONTH),
                1
            )
        } else {
            mCalMonth.set(
                GregorianCalendar.MONTH,
                mCalMonth.get(GregorianCalendar.MONTH) + 1
            )
        }
    }

    private fun setPreviousMonth() {
        if (mCalMonth.get(GregorianCalendar.MONTH) == mCalMonth.getActualMinimum(GregorianCalendar.MONTH)) {
            mCalMonth.set(
                mCalMonth.get(GregorianCalendar.YEAR) - 1,
                mCalMonth.getActualMaximum(GregorianCalendar.MONTH),
                1
            )
        } else {
            mCalMonth.set(GregorianCalendar.MONTH, mCalMonth.get(GregorianCalendar.MONTH) - 1)
        }
    }

    private fun refreshCalendar() {
        mCalendarAdapter.refreshDays()
        mCalendarAdapter.notifyDataSetChanged()
        tv_month.text = format("MMMM yyyy", mCalMonth)
    }

    private fun changeNationFlag(): Drawable? {
        tv_nation.text = mSessionManager?.nation
        return when (mSessionManager?.nation) {
            "Indonesia" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.indonesia, null
            )
            "Singapore" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.singapore, null
            )
            "Malaysia" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.malaysia, null
            )
            "Thailand" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.thailand, null
            )
            "Vietnam" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.vietnam, null
            )
            "Myanmar" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.myanmar, null
            )
            "Cambodia" -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.cambodia, null
            )
            else -> ResourcesCompat.getDrawable(
                resources,
                R.drawable.indonesia, null
            )
        }
    }

    private fun refreshHoliday() {
        val currentDate = mDateFormat.format(mCalMonth.time)
        val listHoliday = getListHoliday(currentDate)
        mHolidayAdapter.data = listHoliday
        mHolidayAdapter.notifyDataSetChanged()
    }

}
