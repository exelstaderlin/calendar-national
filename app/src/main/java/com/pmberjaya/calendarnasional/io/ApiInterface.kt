package com.pmberjaya.calendarnasional.io

import com.pmberjaya.calendarnasional.callback.BaseListCallback
import com.pmberjaya.calendarnasional.model.BannerData
import com.pmberjaya.calendarnasional.model.HariLiburData
import io.reactivex.Observable
import retrofit2.http.*

/**
 * Created by Exel staderlin on 7/12/2017.
 */
object ApiInterface {

    interface MainApiInterface {

        @GET("indonesia")
        fun getIndonesiaJson(): Observable<List<HariLiburData>>

        @GET("singapore")
        fun getSingaporeJson(): Observable<List<HariLiburData>>

        @GET("malaysia")
        fun getMalaysiaJson(): Observable<List<HariLiburData>>

        @GET("thailand")
        fun getThailandJson(): Observable<List<HariLiburData>>

        @GET("myanmar")
        fun getMyanmarJson(): Observable<List<HariLiburData>>

        @GET("vietnam")
        fun getVietnamJson(): Observable<List<HariLiburData>>

        @GET("cambodia")
        fun getCambodiaJson(): Observable<List<HariLiburData>>

        @GET("checkversion")
        fun checkVersion(): Observable<String>


    }

    interface ApiSariputta {

        @GET("api/ads_banner")
        fun getBanner(): Observable<BaseListCallback<BannerData>>
    }


}
