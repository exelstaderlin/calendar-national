package com.pmberjaya.calendarnasional.io

import android.util.Log
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.pmberjaya.calendarnasional.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RestClient {

    private var mApiToken = ""

    fun getMainInterface(token: String): ApiInterface.MainApiInterface {
        mApiToken = token
        val retrofit = createRetrofit(Config.URL, buildOkHttpClient(mApiToken))
        return retrofit.create(ApiInterface.MainApiInterface::class.java)
    }

    fun getApiSariputta(): ApiInterface.ApiSariputta {
        mApiToken = Config.DEFAULT_TOKEN_SARIPUTTA
        val retrofit = createRetrofit(Config.SARIPUTTA_URL, buildOkHttpClient(mApiToken))
        return retrofit.create(ApiInterface.ApiSariputta::class.java)
    }



    private fun createRetrofit(baseUrl: String, okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .also {
                when {
                    BuildConfig.BUILD_TYPE == "debug" -> it.baseUrl(baseUrl)
                    else -> it.baseUrl(baseUrl)
                }
            }
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun buildOkHttpClient(token: String, testInterceptor: Interceptor? = null): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        return OkHttpClient.Builder()
            .addInterceptor(logging)
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor { chain ->
                val original = chain.request()
                val request: Request
                request = original
                    .newBuilder()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .also {
                        if (token.isNotEmpty()) {
                            it.header("Authorization", token)
                        }
                        Log.d("Authorization", token)
                    }
                    .method(original.method(), original.body())
                    .build()

                val response = chain.proceed(request)
                response
            }
            .also {
                if (testInterceptor != null) {
                    it.addInterceptor(testInterceptor)
                }
            }
            .build()
    }


}