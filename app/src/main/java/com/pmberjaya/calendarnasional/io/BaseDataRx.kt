package com.pmberjaya.calendarnasional.io

import com.pmberjaya.calendarnasional.callback.BaseDataCallback
import com.pmberjaya.calendarnasional.callback.BaseListCallback
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.schedulers.Schedulers

class BaseDataRx<T> {
    fun request(
        observable: Observable<BaseDataCallback<T>>,
        disposableObserver: DisposableObserver<BaseDataCallback<T>>
    ): CompositeDisposable {

        return CompositeDisposable().apply {
                add(
                        observable
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribeWith(disposableObserver)
                )
        }

    }

    fun requestList(
        observable: Observable<BaseListCallback<T>>,
        disposableObserver: DisposableObserver<BaseListCallback<T>>
    ) : CompositeDisposable{

        return CompositeDisposable().apply {
                add(
                        observable
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribeOn(Schedulers.computation())
                                .subscribeWith(disposableObserver)
                )
        }
    }

}