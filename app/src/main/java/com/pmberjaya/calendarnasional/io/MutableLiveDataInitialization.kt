package com.pmberjaya.calendarnasional.io

import androidx.lifecycle.MutableLiveData
import kotlin.reflect.KProperty

class MutableLiveDataInitialization<T> {
    var field: MutableLiveData<T> = MutableLiveData()

    operator fun getValue(thisRef: Any?, p: KProperty<*>): MutableLiveData<T> {
        return field
    }

    operator fun setValue(thisRef: Any?, p: KProperty<*>, v: MutableLiveData<T>) {
        field = v
    }

}